# cwe-info-go

This repository contains go bindings which are automatically 
generated from https://gitlab.com/gitlab-org/security-products/cwe-info. 

You can think of this package as a simple, lightweight, self-contained database
that contains CWE information (ID, Title, Description, Solution/Mitigation) 
based on the [`CweInfo` type](https://gitlab.com/gitlab-org/security-products/cwe-info-go/blob/master/cwe_info.go#L14).

In order to get a `CweInfo` object from a given CWE id, you can simply invoke
the [`GetCweInfo`](https://gitlab.com/gitlab-org/security-products/cwe-info-go/blob/master/cwe_info.go#L22) API method.

For getting more information regarding the structure of the CWEs, please have a look at the
[`cwe-info`](https://gitlab.com/gitlab-org/security-products/cwe-info) repository.

# License

The data use to generate these bindings is derived from [MITRE Common Weakness Enumerations (CWE)](https://cwe.mitre.org/). 
The MITRE Corporation grants non-exclusive, royalty-free license to use CWE for research, development, 
and commercial purposes. For more information, please have a look at [Terms.md](./Terms.md) or https://cwe.mitre.org/about/termsofuse.html.