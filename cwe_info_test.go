package cweinfo

import (
	"reflect"
	"testing"
)

func TestCweInfo(t *testing.T) {

	cwe118Info, err := GetCweInfo("118")
	if err != nil {
		t.Errorf("error retrieving CWE-118 %v", err)
	}
	cwe118 := CweInfo{
		ID:          "118",
		Title:       "Incorrect Access of Indexable Resource ('Range Error')",
		Description: "The software does not restrict or incorrectly restricts operations within the boundaries of a resource that is accessed using an index or pointer, such as memory or files.",
		Clusters: []Cluster{
		},
		Solutions: []Solution{
		},
	}
	if !reflect.DeepEqual(cwe118, cwe118Info) {
		t.Errorf("%v is not valid for CWE-118", err)
	}

	cwe14Info, err := GetCweInfo("14")
	if err != nil {
		t.Errorf("error retrieving CWE-14 %v", err)
	}
	cwe14 := CweInfo{
		ID:          "14",
		Title:       "Compiler Removal of Code to Clear Buffers",
		Description: "Sensitive memory is cleared according to the source code, but compiler optimizations leave the memory untouched when it is not read from again, aka \"dead store removal.\"",
		Clusters: []Cluster{
			Cluster{
				Name:  "SFP Secondary",
				Label: "Exposed Data",
			},
			{
				Name: "SFP Primary",
				Label: "Information Leak",
			},
		},
		Solutions: []Solution{
			Solution{
				Title:       "Phase:  Implementation",
				Subtitle:    "",
				Description: []string{"Store the sensitive data in a \"volatile\" memory location if available."},
			},
			Solution{
				Title:       "Phase:  Build and Compilation",
				Subtitle:    "",
				Description: []string{"If possible, configure your compiler so that it does not remove dead stores."},
			},
			Solution{
				Title:       "Phase:  Architecture and Design",
				Subtitle:    "",
				Description: []string{"Where possible, encrypt sensitive data that are used by a software system."},
			},
		},
	}
	if !reflect.DeepEqual(cwe14, cwe14Info) {
		t.Errorf("%v is not valid for CWE-14", err)
	}

	cwe777Info, err := GetCweInfo("777")
	if err != nil {
		t.Errorf("error retrieving CWE-14 %v", err)
	}
	cwe777 := CweInfo{
		ID:          "777",
		Title:       "Regular Expression without Anchors",
		Clusters:    []Cluster{},
		Description: "The software uses a regular expression to perform neutralization, but the regular expression is not anchored and may allow malicious or malformed data to slip through.",
		Solutions: []Solution{
			Solution{
				Title:       "Phase:  Implementation",
				Subtitle:    "",
				Description: []string{"Be sure to understand both what will be matched and what will not be matched by a regular expression. Anchoring the ends of the expression will allow the programmer to define a whitelist strictly limited to what is matched by the text in the regular expression. If you are using a package that only matches one line by default, ensure that you can match multi-line inputs if necessary."},
			},
		},
	}
	if !reflect.DeepEqual(cwe777, cwe777Info) {
		t.Errorf("%v is not valid for CWE-777", err)
	}
}
