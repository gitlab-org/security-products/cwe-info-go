package cweinfo

import (
	"encoding/json"
	"fmt"
)

// Solution represents a solution (mitigation) of a certain vulnerability
type Solution struct {
	Title       string   `json:"title"`
	Subtitle    string   `json:"subtitle"`
	Description []string `json:"description"`
}

// Cluster contains CWE cluster information
type Cluster struct {
	Name  string `json:"name"`
	Label string `json:"label"`
}

// CweInfo represents CWE Information consisting of title, description and solutions (mitigations)
type CweInfo struct {
	ID          string     `json:"id"`
	Title       string     `json:"title"`
	Clusters    []Cluster  `json:"clusters"`
	Description string     `json:"description"`
	Solutions   []Solution `json:"solutions"`
}

// GetCweInfo is a function that, provided the CWE id, returns the corresponding vulnerability information
func GetCweInfo(cweID string) (CweInfo, error) {
	var jsonVar CweInfo
	var vfilename = fmt.Sprintf("CWE-%s.json", cweID)
	var jsonFile, err = Asset(vfilename)
	if err == nil {
		json.Unmarshal(jsonFile, &jsonVar)
	}
	return jsonVar, err
}
